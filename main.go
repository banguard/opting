package main

import (
	"html/template"
	"io"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

// Template se declara
type Template struct {
	templates *template.Template
}

var port = ":8080"

//Host estructura se declara
type (
	Host struct {
		Echo *echo.Echo
	}
)

// Render renders a template document
func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {

	// Add global methods if data is a map
	if viewContext, isMap := data.(map[string]interface{}); isMap {
		viewContext["reverse"] = c.Echo().Reverse
	}

	return t.templates.ExecuteTemplate(w, name, data)
}

func terminos(c echo.Context) error {
	return c.String(http.StatusOK, "terminos")
}

func customHTTPErrorHandler(err error, c echo.Context) {
	httpError, ok := err.(*echo.HTTPError)
	if ok {

		errorCode := httpError.Code
		switch errorCode {
		case http.StatusNotFound:
			//templates.ExecuteTemplate(c.Response().Writer, "404.html", data)
			c.Render(http.StatusNotFound, "404.html", map[string]interface{}{
				"path": c.Request().URL.Path[1:],
			})
		default:
			// TODO handle any other case
		}
	}
}

func main() {
	//genera variable que crea acceso a la carpeta 'templates'
	template := &Template{
		templates: template.Must(template.ParseGlob("templates/*.html")),
	}

	hosts := map[string]*Host{} //declara mapa de string de tipo puntero de Host que almacena subdominios

	//-------------
	// Blog
	//-------------
	blog := echo.New()

	//redireccionamiento desde http:// hacia https:// y desde un https://www hacia un https://
	blog.Pre(middleware.HTTPSRedirect(), middleware.HTTPSNonWWWRedirect())
	//registro de eventos personalizado, mas compacto
	blog.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "time:${time_rfc3339_nano}, remote_ip:${remote_ip}, host:${host}, method=${method}, uri=${uri}, status=${status}\n",
	}))
	blog.Use(middleware.Recover())

	//hosts["blog.banguard.tk"] = &Host{blog}
	hosts["blog.localhost"+port] = &Host{blog}

	blog.Static("/", "assets") //sirve carpeta assets [aunque se podría crear otra para blog]
	blog.Renderer = template   //habilita renderizado de templates en subdominio
	blog.GET("/", func(c echo.Context) error {
		return c.HTML(http.StatusOK, "<h1>Blog</h1>")
	})

	//handler para errores en subdominio "blog"
	blog.HTTPErrorHandler = customHTTPErrorHandler

	//-------------
	// Site
	//-------------
	site := echo.New()

	//redireccionamiento desde http:// hacia https:// y desde un https://www hacia un https://
	site.Pre(middleware.HTTPSRedirect(), middleware.HTTPSNonWWWRedirect())
	//registro de eventos personalizado, mas compacto
	site.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "time:${time_rfc3339_nano}, remote_ip:${remote_ip}, host:${host}, method=${method}, uri=${uri}, status=${status}\n",
	}))
	site.Use(middleware.Recover())

	//hosts["banguard.tk"] = &Host{site}
	hosts["localhost"+port] = &Host{site}
	//sirve carpeta assets [aunque se podría crear otra para blog]
	site.Static("/", "assets") //sirve directorio assets
	site.Renderer = template   //habilita renderizado de templates en subdominio
	//declaracion de handlers
	// site.GET("/contacto", contacto)
	site.POST("/solicitud-contacto", solicitudContacto)
	site.GET("/terminos", terminos)
	site.GET("/solicitud-estudio", solicitudEstudioGet)
	site.POST("/solicitud-estudio", solicitudEstudio)
	site.POST("/solicitud-estudio/sol-rol", solicitudRol)
	//site.GET("/preguntas-frecuentes", preguntasfrecuentes)

	//inyeccion de ruta "/" con server push
	site.GET("/", func(c echo.Context) (err error) { //inicia HTTP/2 server push
		pusher, ok := c.Response().Writer.(http.Pusher)
		if ok {
			if err = pusher.Push("/css/main.css", nil); err != nil { //ficheros a incluir - posicion relativa a index.html
				return
			}
			if err = pusher.Push("/css/bootstrap-4.1.0.min.css", nil); err != nil { //ficheros a incluir - posicion relativa a index.html
				return
			}
			if err = pusher.Push("/js/jquery-3.3.1.min.js", nil); err != nil { //ficheros a incluir - posicion relativa a index.html
				return
			}
			if err = pusher.Push("/favicon.ico", nil); err != nil { //ficheros a incluir - posicion relativa a index.html
				return
			}
			if err = pusher.Push("/js/fontawesome-all-5.0.10.min.js", nil); err != nil { //ficheros a incluir - posicion relativa a index.html
				return
			}
		}
		return c.File("assets/index.html") //index - posicion relativa a root
	})

	site.HTTPErrorHandler = customHTTPErrorHandler //handler para errores en subdominio "site" o en su defecto ""

	//-------------
	// Echo router
	//-------------
	e := echo.New()

	//activa la proteccion xss, content type sniffing, clickjacking, etc
	e.Use(middleware.Secure())
	//compresion gzip
	e.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: 5,
	}))

	//estas lineas deben comentarse cuando se hagan test en localhost
	//e.AutoTLSManager.HostPolicy = autocert.HostWhitelist("banguard.tk", "www.banguard.tk", "blog.banguard.tk", "www.blog.banguard.tk") //crea certificado automatico
	//e.AutoTLSManager.Cache = autocert.DirCache("/var/www/.cache")                                                                      // directorio de certificados
	//e.Use(middleware.Recover())
	// e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
	// 	Format: "time:${time_rfc3339_nano}, remote_ip:${remote_ip}, host:${host}, method=${method}, uri=${uri}, status=${status}\n",
	// }))

	e.Any("/*", func(c echo.Context) (err error) {
		req := c.Request()
		res := c.Response()
		host := hosts[req.Host]

		if host == nil {
			err = echo.ErrNotFound
		} else {
			host.Echo.ServeHTTP(res, req)
		}

		return
	})

	e.Logger.Fatal(e.StartTLS(port, "localhost.crt", "localhost.key")) //iniciador sin auto tls
	// e.Logger.Fatal(e.StartAutoTLS(port)) //iniciador con auto tls
}
