package opt

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	// se usa
	_ "github.com/lib/pq"
)

const (
	hostDB     = "localhost"
	portDB     = 5432
	userDB     = "postgres"
	passwordDB = "dallran3,14"
	dbname     = "opting"
)

// getConnection obtiene conexion con el servidor
func getConnection() (*sql.DB, error) {

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", hostDB, portDB, userDB, passwordDB, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, err
	}
	return db, nil
}

// UserCR defina la información del usuario (Contact Request: CR) enviada a la base de datos.
type UserCR struct {
	FirstName    string
	LastName     string
	Phone        string
	Email        string
	State        string
	City         string
	Subject      string
	Message      string
	CreationDate time.Time
}

//UserSR define la información del usuario (Request Study:RC) enviada a la base de datos.
type UserSR struct {
	Run          string
	Owner        bool
	FirstName    string
	LastName     string
	Phone        string
	Email        string
	State        string
	City         string
	FileName     string
	Message      string
	CreationDate time.Time
}

// CreateConReq crea una solicitud de contacto que registra en DB la información del formulario 'solicitud contacto'
func CreateConReq(cr UserCR) error {
	// declaracion del query
	q := `
	INSERT INTO
		contact_request (first_name, last_name, phone, email, state, city, subject, message, creation_date)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`

	//variables para realizar entradas nulas
	phoneNull := sql.NullString{}

	// conexion con la base de datos (DB)
	db, err := getConnection()
	if err != nil {
		return err
	}
	//se prepara el cierre de la conexion para el final de la funcion
	defer db.Close()

	//se prepara la sentencia (statement)
	stmt, err := db.Prepare(q)
	if err != nil {
		return err
	}

	//se identifica si los valores 'u.phone' y 'u.message' son de cadena vacia, entonces se entrega un nulo en la DB
	if cr.Phone == "" {
		phoneNull.Valid = false
	} else {
		phoneNull.Valid = true
		phoneNull.String = cr.Phone
	}

	//se prepara el cierre para el final de la funcion
	defer stmt.Close()

	//se ejecuta la declaración de variables en la sentencia
	r, err := stmt.Exec(
		cr.FirstName,
		cr.LastName,
		phoneNull,
		cr.Email,
		cr.State,
		cr.City,
		cr.Subject,
		cr.Message,
		cr.CreationDate,
	)
	if err != nil {
		return err
	}

	//determina cuantas filas son afectadas por el INSERT
	i, _ := r.RowsAffected()
	if i != 1 {
		//se crea un nuevo error ya que la condicion 'i != 1' no devuelve uno por defecto, solo un booleano
		return errors.New("ERROR: Se esperaba solo una fila afectada")
	}
	//si todo va bien en vez de un error devuele un nil
	return nil
}

// Create crea una solicitud de contacto que registra en DB la información del formulario 'solicitud estudio'
func Create(rs UserSR) error {
	// declaracion del query
	q := `
		INSERT INTO
			request_study (run, owner, first_name, last_name, phone, email, state, city, filename, message, creation_date)
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`

	//variables para realizar entradas nulas
	phoneNull := sql.NullString{}
	messageNull := sql.NullString{}

	// conexion con la base de datos (DB)
	db, err := getConnection()
	if err != nil {
		return err
	}
	//se prepara el cierre de la conexion para el final de la funcion
	defer db.Close()

	//se prepara la sentencia (statement)
	stmt, err := db.Prepare(q)
	if err != nil {
		return err
	}

	//se identifica si los valores 'u.phone' y 'u.message' son de cadena vacia, entonces se entrega un nulo en la BD
	if rs.Phone == "" {
		phoneNull.Valid = false
	} else {
		phoneNull.Valid = true
		phoneNull.String = rs.Phone
	}
	if rs.Message == "" {
		messageNull.Valid = false
	} else {
		messageNull.Valid = true
		messageNull.String = rs.Message
	}

	//se prepara el cierre para el final de la funcion
	defer stmt.Close()

	//se ejecuta la declaración de variables en la sentencia
	r, err := stmt.Exec(
		rs.Run,
		rs.Owner,
		rs.FirstName,
		rs.LastName,
		phoneNull,
		rs.Email,
		rs.State,
		rs.City,
		rs.FileName,
		messageNull,
		rs.CreationDate,
	)
	if err != nil {
		return err
	}

	//determina cuantas filas son afectadas por el INSERT
	i, _ := r.RowsAffected()
	if i != 1 {
		//se crea un nuevo error ya que la condicion 'i != 1' no devuelve uno por defecto, solo un booleano
		return errors.New("ERROR: Se esperaba solo una fila afectada")
	}
	//si todo va bien en vez de un error devuele un nil
	return nil
}

//ReadRun retorna un booleano sobre existencia de run. Se emplea en solicitud asincrona previa a envio de formulario
func ReadRun(run string) (bool, error) {

	q := `SELECT run FROM request_study WHERE run=$1;`

	//se llama la funcion que realiza la conexion a la base de datos
	db, err := getConnection()
	if err != nil {
		LogError(err)
		return false, err
	}
	//se prepara el cierre de la conexcion para el final de la funcion
	defer db.Close()

	// value to test the no rows use case.
	row := db.QueryRow(q, &run)
	switch err := row.Scan(&run); err {
	case sql.ErrNoRows:
		//no existe registro de rol en DB
		return false, nil
	case nil:
		//existe registro de rol en DB
		return true, nil
	default:
		LogError(err)
		return false, err
	}

}
