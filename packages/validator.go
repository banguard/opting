package opt

//validator.go válida campos del formulario

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/labstack/echo"
)

// Location es una estructura JSON que indica la ubicacion
type Location struct {
	State  string   `json:"state"`
	Cities []string `json:"cities"`
}

var location = []Location{}

// Expresiones regulares básicas
const (
	user          string = `^([a-zA-ZáéíóúüñÁÉÍÓÚÜÑ]+(\s|-)?[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ]+){1,2}$`
	email         string = `^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$`
	rol           string = `^(\d{1,2}\.{1})?\d{3}\.\d{3}[-][0-9kK]{1}$`
	phone         string = `^[0-9]{9}$`
	whiteSpace    string = `\s`
	whiteSpaceDot string = `[\s\.]+`
	dot           string = `\.`
)

var (
	// Funciones para cambiar formato (previo a la validación)
	notWhiteSpace    = regexp.MustCompile(whiteSpace)    // NotWhiteSpace debe ser usado para formatear campos 'email' y 'rol'
	notWhiteSpaceDot = regexp.MustCompile(whiteSpaceDot) //notWhiteSpaceDot
	notDot           = regexp.MustCompile(dot)           // notDot deber ser usado para formatear campo 'rol'
	// Funciones para validar:
	rxUser  = regexp.MustCompile(user) //User must be used to validate names and surnames
	rxEmail = regexp.MustCompile(email)
	rxPhone = regexp.MustCompile(phone)
	rxRol   = regexp.MustCompile(rol)
)

// Owner válida el campo 'owner'
func Owner(s string) bool {
	ow, err := strconv.ParseBool(s)
	if err != nil {
		return false
	}
	return ow
}

//User valida campos 'name' y 'surname'
func User(s string) bool {
	if rxUser.MatchString(strings.TrimSpace(s)) {
		return true
	}
	return false
}

//Email valida campo 'email'
func Email(s string) bool {
	if rxEmail.MatchString(notWhiteSpace.ReplaceAllString(s, "")) {
		return true
	}
	return false
}

// RolNotWhitespaceDot retorna rol de la forma '10000000-K', sin espacios en blanco
func RolNotWhitespaceDot(s string) string {
	return notWhiteSpaceDot.ReplaceAllString(s, "")
}

// RolNotWhitespace retorna rol de la forma '10.000.000-K', sin espacios en blanco
func RolNotWhitespace(s string) string {
	return notWhiteSpace.ReplaceAllString(s, "")
}

//Rol valida campo 'rol', unicamente valida datos de la forma '10.000.000-K'
func Rol(s string) bool {
	// 1. validación por formato (si no es de la forma '10.000.000-K' retorna falso)
	if !rxRol.MatchString(RolNotWhitespace(s)) {
		return false
	}
	// 2. validación por digito verificador
	s = RolNotWhitespaceDot(s)      // reemplaza '.' por nulos
	splitS := strings.Split(s, "-") // separa cadena en un slice de strings de (2) dimensiones por defecto
	var sum uint64
	var mul uint64 = 2
	for i := len(splitS[0]) - 1; i >= 0; i-- {
		charVal, err := strconv.ParseUint(string(splitS[0][i]), 10, 8) //se convierten a entero los caracteres
		if err != nil {
			fmt.Println("hola")
			return false
		}
		sum = sum + charVal*mul
		if mul == 7 { // si el multiplicador ya es igual a 7 se reinicia su valor a 2
			mul = 2
		} else {
			mul = mul + 1
		}
	}
	if verifier(sum) == string(splitS[1]) { // se comparan digito calculado y digito a verificar
		return true
	}
	// 3. validación por existencia en DB
	re, err := ReadRun(s)
	if err != nil || re == true { //recordar que !nil significa que hubo un error en la DB, lo prudente
		// seria separarlos y si es error declararlo y registrarlo
		return false
	}
	return false
}

// Phone válida el campo 'phone'
func Phone(s string) bool {
	if rxPhone.MatchString(notWhiteSpace.ReplaceAllString(s, "")) || s == "" {
		return true
	}
	return false
}

// verifier obtiene el digito verificador (a partir del residuo)
func verifier(sum uint64) string {
	switch sum % 11 {
	case 0:
		return "0"
	case 1:
		return "K"
	default:
		return strconv.FormatUint(11-(sum%11), 10)
	}
}

// stateIndex determina el indice del string buscado en el JSON geodata
func stateIndex(s string) int {
	err := decodeData()
	if err != nil {
		return -1
	}
	for i := 0; i < len(location); i++ {
		if location[i].State == s {
			return i
		}
	}
	return -1
}

// State válida campo 'state'
func State(s string) bool {
	if stateIndex(s) != -1 {
		return true
	}
	return false
}

// StateCity válida campos 'state' y 'city'
func StateCity(state string, city string) bool {
	err := decodeData()
	if err != nil {
		return false
	}
	sc := stateIndex(state)
	if sc == -1 {
		return false
	}
	for i := 0; i < len(location[sc].Cities); i++ {
		if location[sc].Cities[i] == city {
			return true
		}
	}
	return false
}

//Text válida campo 'subject' o 'message' (cuando este sea obligatorio)
func Text(s string) bool {
	if s == "" {
		return false
	}
	return true
}

// File válida campo 'file, realiza la subida y declaracion del archivo
// el segundo campo devuelto corresponde a la ruta del archivo (incluido su nombre)
// el tercer campo devuelto corresponde al nombre del archivo
func File(fi string, now time.Time, c echo.Context) (bool, string, string) {
	// 	verificacion de existencia del directorio que contiene archivos del formulario y creacion de este en caso de no existir
	err := Mkdir("solicitud-estudio", 0700)
	if err != nil {
		return false, "", ""
	}
	file, err := c.FormFile(fi)
	if err != nil {
		return false, "", ""
	}
	// verificación del tamaño de archivo (NO MAYOR QUE 10MB)
	fileSize := file.Size / 1024 / 1024
	if fileSize > 10 {
		return false, "", ""
	}
	// apertura de archivo
	src, err := file.Open()
	if err != nil {
		return false, "", ""
	}
	// declaracion de cierre de 'src'
	defer src.Close()
	// nuevo nombre de archivo a subir
	newFile := "estudio-" + now.Format("06-1-2T15.4.5-0700") + ".rar"
	// ruta de archivo
	pathFile := "solicitud-estudio/" + newFile

	// establece destino de archivo
	dst, err := os.Create(pathFile)
	if err != nil {
		return false, "", ""
	}
	defer dst.Close()

	// copia
	if _, err = io.Copy(dst, src); err != nil {
		return false, "", ""
	}
	// devuelve verdadero, la nueva ruta(con nombre) y nombre
	return true, pathFile, newFile
}

// Mkdir verifica si existe carpeta y crea el directorio si no existe
func Mkdir(name string, perm os.FileMode) error {
	if _, err := os.Stat(name); os.IsNotExist(err) {
		err := os.Mkdir(name, perm)
		if err != nil {
			return err
		}
	}
	return nil
}

// All válida todos los campos
// func All(name string, surname string, rol string, email string, phone string, state string, city string) error {
// }

// decodeData decodifica json
func decodeData() error {
	err := json.Unmarshal(geoData, &location)
	if err != nil {
		LogError(err)
		return err
	}
	return nil
}

// LogError imprime en el archivo 'errors.txt' el error detectado
func LogError(e error) {
	err := Mkdir("logs", 0777)
	if err != nil {
		fmt.Println("Error creando la carpeta")
	}
	file, err := os.OpenFile("logs/errors.txt", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0777)
	if err != nil {
		fmt.Println("No se pudo acceder al archivo")
	}
	defer file.Close()

	log.SetOutput(file)
	log.Println(e)
}

var geoData = []byte(`[
	{
		"state":"Arica y Parinacota",
		"cities":[
			"Arica",
			"Camarones",
			"General Lagos",
			"Putre"
	]
	},
	{
		"state":"Tarapacá",
		"cities":[
			"Alto Hospicio",
			"Camiña",
			"Colchane",
			"Huara",
			"Iquique",
			"Pica",
			"Pozo Almonte"
	]
	},
	{
		"state":"Antofagasta",
		"cities":[
			"Antofagasta",
			"Calama",
			"María Elena",
			"Mejillones",
			"Ollagüe",
			"San Pedro De Atacama",
			"Sierra Gorda",
			"Taltal",
			"Tocopilla"
	]
	},
	{
		"state":"Atacama",
		"cities":[
			"Alto Del Carmen",
			"Caldera",
			"Chañaral",
			"Copiapó",
			"Diego De Almagro",
			"Freirina",
			"Huasco",
			"Tierra Amarilla",
			"Vallenar"
	]
	},
	{
		"state":"Coquimbo",
		"cities":[
			"Andacollo",
			"Canela",
			"Combarbalá",
			"Coquimbo",
			"Illapel",
			"La Higuera",
			"La Serena",
			"Los Vilos",
			"Monte Patria",
			"Ovalle",
			"Paiguano",
			"Punitaqui",
			"Río Hurtado",
			"Salamanca",
			"Vicuña"
	]
	},
	{
		"state":"Valparaíso",
		"cities":[
			"Algarrobo",
			"Cabildo",
			"Calera",
			"Calle Larga",
			"Cartagena",
			"Casablanca",
			"Catemu",
			"Concón",
			"El Quisco",
			"El Tabo",
			"Hijuelas",
			"Isla De Pascua",
			"Juan Fernández",
			"La Calera",
			"La Cruz",
			"La Ligua",
			"Limache",
			"Llaillay",
			"Los Andes",
			"Nogales",
			"Olmué",
			"Panquehue",
			"Papudo",
			"Petorca",
			"Puchuncaví",
			"Putaendo",
			"Quillota",
			"Quilpué",
			"Quintero",
			"Rinconada",
			"San Antonio",
			"San Esteban",
			"San Felipe",
			"Santa María",
			"Santo Domingo",
			"Valparaíso",
			"Villa Alemana",
			"Viña Del Mar",
			"Zapallar"
	]
	},
	{
		"state":"O’Higgins",
		"cities":[
			"Chimbarongo",
			"Chépica",
			"Codegua",
			"Coinco",
			"Coltauco",
			"Doñihue",
			"Graneros",
			"La Estrella",
			"Las Cabras",
			"Litueche",
			"Lolol",
			"Machalí",
			"Malloa",
			"Marchihue",
			"Mostazal",
			"Nancagua",
			"Navidad",
			"Olivar",
			"Palmilla",
			"Paredones",
			"Peralillo",
			"Peumo",
			"Pichidegua",
			"Pichilemu",
			"Placilla",
			"Pumanque",
			"Quinta De Tilcoco",
			"Rancagua",
			"Rengo",
			"Requínoa",
			"San Fernando",
			"San Vicente",
			"Santa Cruz"
	]
	},
	{
		"state":"Maule",
		"cities":[
			"Cauquenes",
			"Chanco",
			"Colbún",
			"Constitución",
			"Curepto",
			"Curicó",
			"Empedrado",
			"Hualañé",
			"Licantén",
			"Linares",
			"Longaví",
			"Maule",
			"Molina",
			"Parral",
			"Pelarco",
			"Pelluhue",
			"Pencahue",
			"Rauco",
			"Retiro",
			"Romeral",
			"Río Claro",
			"Sagrada Familia",
			"San Clemente",
			"San Javier",
			"San Rafael",
			"Talca",
			"Teno",
			"Vichuquén",
			"Villa Alegre",
			"Yerbas Buenas"
	]
	},
	{
		"state":"Biobío",
		"cities":[
			"Alto Biobío",
			"Antuco",
			"Arauco",
			"Bulnes",
			"Cabrero",
			"Cañete",
			"Chiguayante",
			"Chillán",
			"Chillán Viejo",
			"Cobquecura",
			"Coelemu",
			"Coihueco",
			"Concepción",
			"Contulmo",
			"Coronel",
			"Curanilahue",
			"El Carmen",
			"Florida",
			"Hualpén",
			"Hualqui",
			"Laja",
			"Lebu",
			"Los Álamos",
			"Los Ángeles",
			"Lota",
			"Mulchén",
			"Nacimiento",
			"Negrete",
			"Ninhue",
			"Ñiquén",
			"Pemuco",
			"Penco",
			"Pinto",
			"Portezuelo",
			"Quilaco",
			"Quilleco",
			"Quillón",
			"Quirihue",
			"Ránquil",
			"San Carlos",
			"San Fabián",
			"San Ignacio",
			"San Nicolás",
			"San Pedro De La Paz",
			"San Rosendo",
			"Santa Bárbara",
			"Santa Juana",
			"Talcahuano",
			"Tirúa",
			"Tomé",
			"Treguaco",
			"Tucapel",
			"Yumbel",
			"Yungay"
	]
	},
	{
		"state":"La Araucanía",
		"cities":[
			"Angol",
			"Carahue",
			"Cholchol",
			"Collipulli",
			"Cunco",
			"Curacautín",
			"Curarrehue",
			"Ercilla",
			"Freire",
			"Galvarino",
			"Gorbea",
			"Lautaro",
			"Loncoche",
			"Lonquimay",
			"Los Sauces",
			"Lumaco",
			"Melipeuco",
			"Nueva Imperial",
			"Padre Las Casas",
			"Perquenco",
			"Pitrufquén",
			"Pucón",
			"Purén",
			"Renaico",
			"Saavedra",
			"Temuco",
			"Teodoro Schmidt",
			"Toltén",
			"Traiguén",
			"Victoria",
			"Vilcún",
			"Villarrica"
	]
	},
	{
		"state":"Los Ríos",
		"cities":[
			"Corral",
			"Futrono",
			"La Unión",
			"Lago Ranco",
			"Lanco",
			"Los Lagos",
			"Mariquina",
			"Máfil",
			"Paillaco",
			"Panguipulli",
			"Río Bueno",
			"Valdivia"
	]
	},
	{
		"state":"Los Lagos",
		"cities":[
			"Ancud",
			"Calbuco",
			"Castro",
			"Chaitén",
			"Chonchi",
			"Cochamó",
			"Curaco De Vélez",
			"Dalcahue",
			"Fresia",
			"Frutillar",
			"Futaleufú",
			"Hualaihué",
			"Llanquihue",
			"Los Muermos",
			"Maullín",
			"Osorno",
			"Palena",
			"Puerto Montt",
			"Puerto Octay",
			"Puerto Varas",
			"Puqueldón",
			"Purranque",
			"Puyehue",
			"Queilén",
			"Quellón",
			"Quemchi",
			"Quinchao",
			"Río Negro",
			"San Juan De La Costa",
			"San Pablo"
	]
	},
	{
		"state":"Aisén",
		"cities":[
			"Aysén",
			"Chile Chico",
			"Cisnes",
			"Cochrane",
			"Coyhaique",
			"Guaitecas",
			"Lago Verde",
			"O’Higgins",
			"Río Ibáñez",
			"Tortel"
	]
	},
	{
		"state":"Magallanes",
		"cities":[
			"Antártica",
			"Cabo de Hornos",
			"Laguna Blanca",
			"Natales",
			"Porvenir",
			"Primavera",
			"Punta Arenas",
			"Río Verde",
			"San Gregorio",
			"Timaukel",
			"Torres Del Paine"
	]
	},
	{
		"state":"Metropolitana",
		"cities":[
			"Alhué",
			"Buin",
			"Calera De Tango",
			"Cerrillos",
			"Cerro Navia",
			"Colina",
			"Conchalí",
			"Curacaví",
			"El Bosque",
			"El Monte",
			"Estación Central",
			"Huechuraba",
			"Independencia",
			"Isla de Maipo",
			"La Cisterna",
			"La Florida",
			"La Granja",
			"La Pintana",
			"La Reina",
			"Lampa",
			"Las Condes",
			"Lo Barnechea",
			"Lo Espejo",
			"Lo Prado",
			"Macul",
			"Maipú",
			"María Pinto",
			"Melipilla",
			"Ñuñoa",
			"Padre Hurtado",
			"Paine",
			"Pedro Aguirre Cerda",
			"Peñaflor",
			"Peñalolén",
			"Pirque",
			"Providencia",
			"Pudahuel",
			"Puente Alto",
			"Quilicura",
			"Quinta Normal",
			"Recoleta",
			"Renca",
			"San Bernardo",
			"San Joaquín",
			"San José De Maipo",
			"San Miguel",
			"San Pedro",
			"San Ramón",
			"Santiago",
			"Talagante",
			"Til til",
			"Vitacura"
	]
	}
]`)
