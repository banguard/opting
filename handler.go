package main

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/opting/packages"
	"github.com/labstack/echo"
)

func solicitudContacto(c echo.Context) error {
	// req := c.Request() //obtiene el request (req) de contexto echo en formato *Request
	// if req.Method != "POST" { //verifica si el metodo del request (req) [string] es distinto a "POST"
	// 	return c.Redirect(http.StatusSeeOther, "/") //retorna a la pagina de inicio si es verdad
	// }
	//extrae datos desde formulario
	name := strings.Title(c.FormValue("name"))
	surname := strings.Title(c.FormValue("surname"))
	phone := c.FormValue("phone")
	email := strings.ToLower(c.FormValue("email"))
	state := strings.Title(c.FormValue("state"))
	city := strings.Title(c.FormValue("city"))
	subject := strings.Title(c.FormValue("subject"))
	message := c.FormValue("message")
	now := time.Now()
	// fmt.Println(name, surname, phone, email, state, city, subject, message)
	//verificacion de validez de datos
	if !opt.User(name) ||
		!opt.User(surname) ||
		!opt.Phone(phone) ||
		!opt.Email(email) ||
		!opt.StateCity(state, city) ||
		!opt.Text(subject) ||
		!opt.Text(message) {
		fmt.Println(opt.User(name), opt.User(surname), opt.Phone(phone), opt.Email(email), opt.StateCity(state, city), opt.Text(subject), opt.Text(message))
		opt.LogError(errors.New("Enviado al servidor un formulario de contacto con uno o mas campos no válidos"))
		return c.Redirect(http.StatusSeeOther, "/") // CREAR TEMPLATE QUE INDIQUE ERROR EN SOLICITUD
	}
	cr := opt.UserCR{
		FirstName:    name,
		LastName:     surname,
		Phone:        phone,
		Email:        email,
		State:        state,
		City:         city,
		Subject:      subject,
		Message:      message,
		CreationDate: now,
	}
	// genera en la BD un nuevo registro de solicitud
	if err := opt.CreateConReq(cr); err != nil {
		opt.LogError(err)
		return c.Render(http.StatusBadRequest, "sol-no-pro.html", nil) //MEJORAR RESPUESTA
	}

	// envio email de notificacion a personal de OPTING
	err := conMail(name, surname, phone, email, city, subject, message)
	if err != nil {
		opt.LogError(err)
	}
	// envia email de notificacion a usuario
	err = conMailCl(name, surname, email)
	if err != nil {
		opt.LogError(err)
	}
	//DESARROLLAR UNA RESPUESTA AJAX EN VEZ DE REDIRECCIONAMIENTO
	//devuelve la ejecucion del template de contacto procesado
	return c.Render(http.StatusOK, "con-pro.html", map[string]interface{}{
		"name":    name,
		"surname": surname,
		"email":   email,
	})
}

func solicitudEstudioGet(c echo.Context) error {
	return c.Render(http.StatusOK, "solicitud-estudio.html", nil)
}

func solicitudEstudio(c echo.Context) error {
	// req := c.Request()

	//este método es una alternativa a la extraccion individual de datos. Devuelve un puntero de mapa de string
	// data, err := c.MultipartForm()
	// if err != nil {
	// 	return c.String(http.StatusBadRequest, "error en leer")
	// }
	// fmt.Println(data)

	//extraccion de datos desde formulario
	ownerString := c.FormValue("radio-ownership")
	name := strings.Title(c.FormValue("name"))
	surname := strings.Title(c.FormValue("surname"))
	rol := strings.ToUpper(c.FormValue("rol"))
	phone := c.FormValue("phone")
	email := strings.ToLower(c.FormValue("email"))
	state := strings.Title(c.FormValue("state"))
	city := strings.Title(c.FormValue("city"))
	message := c.FormValue("message")
	//hora de registro de formulario
	now := time.Now()

	//calculo y transformación de string a booleano de variable 'owner'
	owner := opt.Owner(ownerString)

	//verificacion de validez de datos
	if !opt.User(name) ||
		!opt.User(surname) ||
		!opt.Rol(rol) ||
		!opt.Phone(phone) ||
		!opt.Email(email) ||
		!opt.StateCity(state, city) {
		return c.Redirect(http.StatusSeeOther, "/") // CREAR TEMPLATE QUE INDIQUE ERROR EN SOLICITUD
	}
	// verificacion de validez de archivo
	fileVal, pathFile, newFile := opt.File("file", now, c)
	if !fileVal {
		return c.Redirect(http.StatusSeeOther, "/")
	}

	//reemplazo de rol a uno sin puntos (de 9.999.999-K a 9999999-K)
	rol = opt.RolNotWhitespaceDot(rol)

	// registro en DB
	sr := opt.UserSR{
		Run:          rol,
		Owner:        owner,
		FirstName:    name,
		LastName:     surname,
		Phone:        phone,
		Email:        email,
		State:        state,
		City:         city,
		FileName:     newFile,
		Message:      message,
		CreationDate: now,
	}

	// genera en la BD un nuevo registro de solicitud
	if err := opt.Create(sr); err != nil {
		opt.LogError(err)
		return c.Render(http.StatusBadRequest, "sol-no-pro.html", nil) //MEJORAR RESPUESTA
	}

	// envio email de notificacion a personal de OPTING
	err := solMail(name, surname, rol, phone, email, state, city, message, pathFile)
	if err != nil {
		opt.LogError(err)
	}
	// envia email de notificacion a usuario
	err = solMailCl(name, surname, email)
	if err != nil {
		opt.LogError(err)
	}
	//devuelve la ejecucion de template de contacto procesado
	return c.Render(http.StatusOK, "sol-pro.html", map[string]interface{}{
		"name":    name,
		"surname": surname,
		"email":   email,
	})
}

func solicitudRol(c echo.Context) error {

	req := c.Request()
	res := c.Response()
	//se obtiene el rol desde /solicitud-estudio
	bs, err := ioutil.ReadAll(req.Body)
	if err != nil {
		opt.LogError(err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	rol := opt.RolNotWhitespaceDot(string(bs))
	//re (run exists) devuelve un booleano
	re, err := opt.ReadRun(rol)
	if err != nil {
		opt.LogError(err)
		return nil //MEJORAR RESPUESTA
	}
	//envia respuesta al cliente (true: existe rol, false: no existe rol)
	io.WriteString(res, strconv.FormatBool(re))

	return nil
}
