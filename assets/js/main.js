// animacion y transición de carga de página
document.getElementById("body").onload = body()
function body() {
  setTimeout(loading, 0500)
}
function loading() {
  document.getElementById("loader").style.display = "none"
  document.getElementById("mainpage").style.display = "block"
}

$(document).ready(function () {
  $("#scrolldown").click(function () {
    $('html, body').animate({
      scrollTop: $("#inicio").offset().top
    }, 1000)
  })
})

function _(id) {
  return document.getElementById(id)
}

var elemento = new Array(10)
var arrID = new Array(10)
var nameReg = new RegExp(/^([a-zA-ZáéíóúüñÁÉÍÓÚÜÑ]+(\s|-)?[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ]+){1,2}$/) //{1,2} permite dos espacios
var phoneReg = new RegExp(/^[0-9]{9}$/)
var emailReg = new RegExp(
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
)
var feedbackFile
var arrID = ["name", "surname", "phone", "email", "emailCo", "state", "city", "subject", "message", "terms"]

function ix(id) {
  return arrID.indexOf(id)
}

//se detiene el envío del formulario
function detenerEnvio() {
  event.preventDefault();
  event.stopPropagation();
}
//variable que registra JSON con los datos geograficos
var obj = JSON.parse(geodata);

//pre-carga la lista regiones
regiones()
//pre-carga lista unica de comuna
_("city").innerHTML = "<option>Seleccionar Comuna</option>"
//capitaliza la palabras
function toTitleCase(str) {
  return str.replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
  })
}
//remueve espacios en blanco a la izquierda, dos o mas espacios en cualquier otro lugar y caracteres especiales
function lTrim(id) {
  idValue = _(id).value
  if (idValue == null) return idValue;
  return idValue.replace(/(^(\s)+|\s\s+|[^a-zA-ZáéíóúüñÁÉÍÓÚÜÑ\s\-]+)/, "")
}
//remueve todo caracter distinto de numero
function onlyNumber(id) {
  idValue = _(id).value
  if (idValue == null) return idValue;
  return idValue.replace(/[^0-9]+/, "")
}
//remocion de todo espacio en blanco
function noWhite(id) {
  idValue = _(id).value
  if (idValue == null) return idValue;
  return idValue.replace(/\s+/, "")
}

//validacion de campo 'name'
function nombreVal() {
  _("name").value = toTitleCase(_("name").value.trim())

  if (nameReg.test(_("name").value)) {
    elemento[ix("name")] = true
  } else {
    elemento[ix("name")] = false
  }
}
//validacion de campo 'surname'
function apellidoVal() {
  _("surname").value = toTitleCase(_("surname").value.trim())
  if (nameReg.test(_("surname").value)) {
    elemento[ix("surname")] = true
  } else {
    elemento[ix("surname")] = false
  }

}
//validacion de campo 'phone'
function telefonoVal() {
  if (phoneReg.test(_("phone").value) || _("phone").value === "") {
    elemento[ix("phone")] = true
  } else {
    elemento[ix("phone")] = false
  }
}

function emailVal() {
  _("email").value = noWhite("email").toLowerCase()
  if (emailReg.test(_("email").value)) {
    elemento[ix("email")] = true;
  } else {
    elemento[ix("email")] = false;
  }
}
function emailCoVal() {
  _("emailCo").value = noWhite("emailCo").toLowerCase()
  if (_("email").value === _("emailCo").value && _("email").value != "") {
    elemento[ix("emailCo")] = true;
  } else if (_("email").value === _("emailCo").value && _("email").value == "" || _("email").value == "") {
    elemento[ix("emailCo")] = false;
    _("emailCoFeedback").innerText = "Ingresa primero un correo válido"
  } else {
    elemento[ix("emailCo")] = false;
    _("emailCoFeedback").innerText = "Los correos no coinciden"
  }
}

function regionVal() {
  var state = _("state")
  if (state.options[state.selectedIndex].index != 0) {
    elemento[ix("state")] = true
  } else {
    elemento[ix("state")] = false
  }
}

function comunaVal() {
  var city = _("city")
  if (city.options[city.selectedIndex].index != 0) {
    elemento[ix("city")] = true
  } else {
    elemento[ix("city")] = false
  }
}
function subjectVal() {
  _("subject").value = toTitleCase(_("subject").value)
  if (_("subject").value == "") {
    elemento[ix("subject")] = false
  } else {
    elemento[ix("subject")] = true
  }
}
function messageVal() {
  if (_("message").value == "") {
    elemento[ix("message")] = false
  } else {
    elemento[ix("message")] = true
  }
}

function terminosVal() {

  if (_("terms").checked === true) {
    elemento[ix("terms")] = true
  } else {
    elemento[ix("terms")] = false
  }
}


//elID corresponde a cual dato (input) se quiere validar 0,1,2,3,4,5 o true si se quieren todos
function validation(elID) {

  for (var i = 0; i < arrID.length; i++) {
    //si el id es distinto al que se quiere evaluar y si no se quieren todos, se salta 'i'
    if (elID !== i && elID !== true) {
      continue
    }

    var arrIdElement = _(arrID[i]);

    if (elemento[i] == false) {
      detenerEnvio()
      arrIdElement.classList.remove("is-valid")
      arrIdElement.classList.add("is-invalid")
    } else {
      arrIdElement.classList.remove("is-invalid")
      if ((i == ix("phone") && _("phone").value == "")) {
        continue
      }
      arrIdElement.classList.add("is-valid")
    }
  }

}
//pre-carga lista de regiones
function regiones() {
  var state = _("state")
  state.innerHTML = "<option>Seleccionar Región</option>"
  for (let i = 0; i < obj.regiones.length; i++) {
    state.innerHTML += "<option>" + obj.regiones[i].NombreRegion + "</option>"
  }
}
//carga lista de comunas ante cambio de region
function comunas(reg) {
  var city = _("city")
  city.innerHTML = "<option>Seleccionar Comuna</option>"
  // si se selecciona "Seleccionar Región" se devuelve el valor ya establecido (Seleccionar Comuna)
  if (reg == -1) return

  for (let i = 0; i < obj.regiones[reg].comunas.length; i++) {
    city.innerHTML += "<option>" + obj.regiones[reg].comunas[i] + "</option>"
  }
}


_("name").addEventListener("blur", function () {
  nombreVal()
  validation(ix("name"))
})
_("surname").addEventListener("blur", function () {
  apellidoVal()
  validation(ix("surname"))
})
_("phone").addEventListener("blur", function () {
  telefonoVal()
  validation(ix("phone"))
})

_("email").addEventListener("blur", function () {
  emailVal()
  validation(ix("email"))
  emailCoVal()
  validation(ix("emailCo"))
})
_("emailCo").addEventListener("blur", function () {
  emailCoVal()
  validation(ix("emailCo"))
})

_("state").addEventListener("change", function () {
  var regionsel = this.options[this.selectedIndex].index
  // (regionsel-1) debido a la adicion que representa colocar la opcion "Seleccionar Region"
  comunas(regionsel - 1)
  regionVal()
  validation(ix("state"))
})
_("city").addEventListener("change", function () {
  comunaVal()
  validation(ix("city"))
})
_("subject").addEventListener("blur", function () {
  subjectVal()
  validation(ix("subject"))
})
_("message").addEventListener("blur", function () {
  messageVal()
  validation(ix("message"))
})
_("terms").addEventListener("change", function () {
  terminosVal()
  validation(ix("terms"))
})

_("enviar").addEventListener("click", function () {

  //realiza el barrido de datos analizando su cumplimiento
  nombreVal()
  apellidoVal()
  telefonoVal()
  emailVal()
  emailCoVal()
  regionVal()
  comunaVal()
  subjectVal()
  messageVal()
  terminosVal()

  //realiza validacion en todos los campos
  validation(true)

  // _("sending-icon").style.display = "block"
  // _("sending-text").innerHTML = "Enviando..."
  //_("conForm").submit()
})


