
rut = document.getElementById("rut")

//obtiene rut
rut.addEventListener("blur",function(){
	console.log(onlyNumber(rut.value))
	rutSDv = parseInt(onlyNumber(rut.value), 10)
	console.log(computeDv(rut))
})

//dejan solo numeros
function onlyNumber(value) {
	return value.replace(/[\.\-]/g, "");
}


function isValidRut(rut, options) {
	if(typeof(rut) !== 'string') { return false; }
	var cRut = clearFormat(rut);
	// validar por largo mínimo, sin guiones ni puntos:
	// x.xxx.xxx-x
	if ( typeof options.minimumLength === 'boolean' ) {
		if ( options.minimumLength && cRut.length < defaults.minimumLength ) {
			return false;
		}
	} else {
		var minLength = parseInt( options.minimumLength, 10 );
		if ( cRut.length < minLength ) {
			return false;
		}
	}
	var cDv = cRut.charAt(cRut.length - 1).toUpperCase();
	var nRut = parseInt(cRut.substr(0, cRut.length - 1));
	if(isNaN(nRut)){ return false; }
	return computeDv(nRut).toString().toUpperCase() === cDv;
}


//determina el digito verificador, el dato que se le pasa no debe tener digito verificador
function computeDv(rut) {
	var suma	= 0;
	var mul		= 2;
	if(typeof(rut) !== 'number') { return; }
	rut = rut.toString();
	for(var i=rut.length -1;i >= 0;i--) {
		suma = suma + rut.charAt(i) * mul;
		mul = ( mul + 1 ) % 8 || 2;
	}
	switch(suma % 11) {
		case 1	: return 'k';
		case 0	: return 0;
		default	: return 11 - (suma % 11);
	}
}