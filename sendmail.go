package main

import (
	"gopkg.in/gomail.v2"
)

const (
	serverMail string = "smtp.gmail.com"
	portMail   int    = 465
	mail       string = "optingingenieria@gmail.com"
	pass       string = "2236optingingenieria"
)

func conMail(name string, surname string, phone string, email string, city string, subject string, message string) error {

	m := gomail.NewMessage(gomail.SetCharset("UTF-8"))
	m.SetHeader("From", "optingingenieria@gmail.com")
	m.SetHeader("To", "klbohle@gmail.com")
	// m.SetAddressHeader("Cc", "dan@example.com", "Dan")
	m.SetHeader("Subject", "OPTING: Se realizó una solicitud de contacto")
	m.SetBody("text/html",
		`<p>Nombre: <b>`+name+`</b></p>
		<p>Apellido: <b>`+surname+`</b></p>
		<p>Telefono: <b>`+phone+`</b></p>
		<p>Email: <b>`+email+`</b></p>
		<p>Comuna: <b>`+city+`</b></p>
		<p>Asunto: <b>`+subject+`</b></p>
		<p>Mensaje: <b>`+message+`</b></p>
	`)

	d := gomail.NewDialer(serverMail, portMail, mail, pass)

	// Envia el email a los destinatarios
	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}
func conMailCl(name string, surname string, email string) error {
	m := gomail.NewMessage(gomail.SetCharset("UTF-8"))
	m.SetHeader("From", "optingingenieria@gmail.com")
	m.SetHeader("To", m.FormatAddress(email, name+" "+surname))
	// m.SetAddressHeader("Cc", "dan@example.com", "Dan")
	m.SetHeader("Subject", "OPTING: Solicitud de Contacto Recibida")
	m.Embed("assets/favicon.ico")
	m.SetBody("text/html",
		`
		<html>
			<head>
				<style>
					body {
						background: #f4f4f4;
					}
					img {
						width: 3em;
						height: auto
					}
					.text {
						text-align: justify;
						color: #282b43
					}
					hr {
						border: #282b43 1px solid;
					}
					.bottom {
						font-size: 0.7em;
						text-align: center
					}
				</style>
			</head>
			<body>
				<img src="cid:favicon.ico" alt="OptIng.cl"/>
				<p>Estimado/a <b>`+name+` `+surname+`</b>:</p>
				<p class="text">Hemos recibido tu solicitud de contacto, Muy pronto, dentro de un horario hábil, alguien de nuestro equipo se encargará de resolver tus dudas.</p>
				<hr>
				<p class="bottom">La información contenida en éste documento es confidencial. Si recibiste este correo es porque lo solicitaste en <a href="https://opting.cl/" target="_blank">opting.cl</a>. Si no solicitaste el contacto favor reenvianos una copia a esta dirección <a href="mailto:optingingenieria@gmail.com">optingingenieria@gmail.com</a></p>
				</body>
		</html>
		`)

	d := gomail.NewDialer(serverMail, portMail, mail, pass)

	// Envia el email a los destinatarios
	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}

func solMail(name string, surname string, rol string, phone string, email string, state string, city string, message string, fileName string) error {
	m := gomail.NewMessage(gomail.SetCharset("UTF-8"))
	m.SetHeader("From", "optingingenieria@gmail.com")
	m.SetHeader("To", "klbohle@gmail.com")
	// m.SetAddressHeader("Cc", "dan@example.com", "Dan")
	m.SetHeader("Subject", "OPTING: Se realizó una solicitud de estudio")
	m.SetBody("text/html",
		`<p style="text-justify:center">Un usuario de <b>opting.cl</b> solicitó un estudio de reactivos, se adjunta al final del documento un archivo comprimido con su información de facturación:</p>
		<p>Nombre: <b>`+name+`</b></p>
		<p>Apellido: <b>`+surname+`</b></p>
		<p>Rol: <b>`+rol+`</b></p>
		<p>Telefono: <b>`+phone+`</b></p>
		<p>Email: <b>`+email+`</b></p>
		<p>Región: <b>`+state+`</b></p>
		<p>Comuna: <b>`+city+`</b></p>
		<p>Mensaje: <b>`+message+`</b></p>
	`)
	m.Attach(fileName)

	d := gomail.NewDialer(serverMail, portMail, mail, pass)

	// Envia el email a los destinatarios
	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}

func solMailCl(name string, surname string, email string) error {
	m := gomail.NewMessage(gomail.SetCharset("UTF-8"))
	m.SetHeader("From", "optingingenieria@gmail.com")
	m.SetHeader("To", m.FormatAddress(email, name+" "+surname))
	// m.SetAddressHeader("Cc", "dan@example.com", "Dan")
	m.SetHeader("Subject", "OPTING: Solicitud de Estudio Recibida")
	m.Embed("assets/favicon.ico")
	m.SetBody("text/html",
		`
		<html>
			<head>
				<style>
					body {
						background: #f4f4f4;
					}
					img {
						width: 3em;
						height: auto
					}
					.text {
						text-align: justify;
						color: #282b43
					}
					hr {
						border: #282b43 1px solid;
					}
					.bottom {
						font-size: 0.7em;
						text-align: center
					}
				</style>
			</head>
			<body>
				<img src="cid:favicon.ico" alt="OptIng.cl"/>
				<p>Estimado/a <b>`+name+` `+surname+`</b>:</p>
				<p class="text">Hemos recibido tu solicitud de estudio, Muy pronto, dentro de un horario hábil, alguien de nuestro equipo se encargará de evaluarla y elaborará un informe preliminar que te mostrará las distintas alternativas presupuestarias.</p>
				<hr>
				<p class="bottom">La información contenida en éste documento es confidencial. Si recibiste este correo es porque lo solicitaste en <a href="https://opting.cl/solicitud-estudio.html" target="_blank">opting.cl</a>. Si lo recibiste por equivocación por favor reenvianos una copia a esta dirección <a href="mailto:optingingenieria@gmail.com">optingingenieria@gmail.com</a></p>
				</body>
		</html>
		`)

	d := gomail.NewDialer(serverMail, portMail, mail, pass)

	// Envia el email a los destinatarios
	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}
